﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(turdova.Startup))]
namespace turdova
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
