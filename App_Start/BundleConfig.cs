﻿using System.Web;
using System.Web.Optimization;

namespace turdova
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery.min.js",
                        "~/Scripts/jquery.bootstrap.wizard.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                        "~/Scripts/angular.js",
                        "~/Scripts/angular-route.js",
                        "~/Scripts/ui-bootstrap-tpls.js",
                        "~/Scripts/angular-resource.js",
                          "~/Scripts/underscore.js",
                          "~/Scripts/angular-aria.min.js",
                          "~/Scripts/angular-material.min.js",
                          "~/Scripts/angular-animate.js"
                          //"~/Scripts/material-bootstrap-wizard.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/bootstrap.min.css",
                      "~/Content/angular-material.min.css",
                      "~/Content/font-awesome.css",
                      "~/Content/stepwizard.css",
                      "~/Content/style.css",
                       "~/Content/material-bootstrap-wizard.css"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                    "~/Scripts/app/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/appdependencies").Include(
                      "~/Scripts/app/CustomData/*.js"));
        }
    }
}
