/// <autosync enabled="true" />
/// <reference path="angular.min.js" />
/// <reference path="angular-animate.min.js" />
/// <reference path="angular-aria.min.js" />
/// <reference path="angular-material.min.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-resource.min.js" />
/// <reference path="angular-route.min.js" />
/// <reference path="app/app.js" />
/// <reference path="app/customdata/colordirective.js" />
/// <reference path="app/customdata/controlsdirective.js" />
/// <reference path="app/customdata/dynamictemplatecontroller.js" />
/// <reference path="app/customdata/homecontroller.js" />
/// <reference path="app/customdata/inheritencedirective.js" />
/// <reference path="app/customdata/mopuseclickedcontroller.js" />
/// <reference path="app/customdata/multiselectdirective.js" />
/// <reference path="app/customdata/multiselectdirective2.js" />
/// <reference path="app/customdata/samplecontroller.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery.bootstrap.wizard.js" />
/// <reference path="jquery.min.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="material-bootstrap-wizard.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="offline.js" />
/// <reference path="respond.js" />
/// <reference path="ui-bootstrap.min.js" />
/// <reference path="ui-bootstrap-tpls.min.js" />
/// <reference path="underscore.min.js" />
