﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('homecontroller', homecontroller);

    homecontroller.$inject = ['$location']; 

    function homecontroller($location) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'homecontroller';

        activate();

        function activate() { }
    }
})();
