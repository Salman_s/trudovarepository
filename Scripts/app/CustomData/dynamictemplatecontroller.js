﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('dynamictemplatecontroller', dynamictemplatecontroller);
    
    dynamictemplatecontroller.$inject = ['$location', '$scope'];

    function dynamictemplatecontroller($location, $scope) {
        /* jshint validthis:true */
        var vm = this;
        vm.title = 'dynamictemplatecontroller';

        activate();

        function activate() {
            $scope.example14model = [];
            $scope.example14settings = {
                scrollableHeight: '200px',
                scrollable: true,
                enableSearch: true
            };
            $scope.example2settings = {
                displayProp: 'CodeID'
            };
            $scope.gender = [{ CodeID: 1, Code: "Male" }, { CodeID: 2, Code: "Female" }];

            $scope.fieldData = [
            {
                "displayName": "Name", "field": "txtName", "mandatorymark": true, "type": "text"
            },
            {
                "displayName": "Age", "field": "txtAge", "mandatorymark": false, "type": "text"
            },
            {
                "displayName": "Gender", "field": "ddlgender", "mandatorymark": true, "type": "genderselect"
            }
            ];
           

        }
    }
})();
