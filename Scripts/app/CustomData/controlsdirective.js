﻿(function() {
    'use strict';

    angular
        .module('app')
        .directive('controlsDirective', controlsdirective);

    controlsdirective.$inject = ['$window'];
    
    function controlsdirective ($window) {
        // Usage:
        //     <controlsdirective></controlsdirective>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'E',
            scope: {
                giventext: '='
            },
            template: '<input type="text" ng-model="giventext"/>'
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }

})();