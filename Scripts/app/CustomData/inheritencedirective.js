﻿(function () {
    'use strict';

    angular
        .module('app')
        .directive('mouseClicked', mouseClicked)
        .directive('ebook', ebook)
        .directive('magazine', magazine);

    magazine.$inject = ['$window']
    ebook.$inject = ['$window'];
    mouseClicked.$inject = ['$window'];

    function mouseClicked($window) {
        // Usage:
        //     <inheritencedirective></inheritencedirective>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
            scope: {},
            controller: "MouseClickedCtrl as mouseClicked"
        };
        return directive;

        function link(scope, element, attrs) {
        }
    }
    function ebook($window) {
        return {
            require: "mouseClicked",
            link: function (scope, element, attrs, mouseClickedCtrl) {
                mouseClickedCtrl.setBookType("EBOOK");
            }
        }
    }
    function magazine($window) {
        return {
            require: "mouseClicked",
            link: function (scope, element, attrs, mouseClickedCtrl) {
                mouseClickedCtrl.setBookType("MAGAZINE");
            }
        }
    }
})();