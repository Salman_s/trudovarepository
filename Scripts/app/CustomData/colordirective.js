﻿(function() {
    'use strict';

    angular
        .module('app')
        .directive('colorDirective', colorDirective);

    colorDirective.$inject = ['$window'];
    
    function colorDirective ($window) {
        // Usage:
        //     <colordirective></colordirective>
        // Creates:
        // 
        var directive = {
            link: link,
            restrict: 'EA',
           
            template: '<p ng-style="{background: color}">color: {{color}}</p>',
          //  template: '<p style="background-color:{{color}}">Hello World</p>',
            
           
        };
        return directive;

        function link(scope, elem, attrs) {
            elem.bind('click', function () {
                elem.css('background-color', 'red');
                scope.$apply(function () {
                    scope.color = "red";
                });
            });
            elem.bind('mouseover', function () {
                elem.css('cursor', 'pointer');
            });
        }
    }

})();