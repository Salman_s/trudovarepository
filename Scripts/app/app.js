﻿var app = angular.module('app', ['ngRoute', 'ui.bootstrap', 'ngMaterial', 'ngAnimate'
   ]);
app.config(['$routeProvider', function ($routeProvider, $locationProvider) {
    // new deplosyment 
    $routeProvider
    // route for the Home page
        .when('/', {
            templateUrl: '/Tmpls/Home.html',
            controller: 'homecontroller',
            title: '',
        })
         .when('/pagedetails', {
             controller: 'samplecontroller',
             templateUrl: '/Tmpls/Details.html',
             title: '',

         })
        .when('/viewquotes', {
            controller: 'samplecontroller',
            templateUrl: '/Tmpls/viewquotes.html',
            title: '',

        })
         

       .otherwise({ redirectTo: '/' });

}])